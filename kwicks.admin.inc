<?php


/**
 * Kwicks settings form.
 */
function kwicks_admin() {
  $form['kwicks_number'] = array(
    '#type' => 'textfield',
    '#description' => t('Number of Kwicks blocks that will be generated.'),
    '#default_value' => variable_get('kwicks_number', 1),
    '#size' => 2,
  );
  return system_settings_form($form);
}